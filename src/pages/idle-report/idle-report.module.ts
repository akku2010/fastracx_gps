import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdleReportPage } from './idle-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    IdleReportPage,
  ],
  imports: [
    IonicPageModule.forChild(IdleReportPage),
    SelectSearchableModule
  ],
})
export class IdleReportPageModule {}
