import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-your-trips',
  templateUrl: 'your-trips.html',
})
export class YourTripsPage {
  islogin: any;
  datetimeStart: any;
  datetimeEnd: any;
  portstemp: any[] = [];
  trip_id: any;
  _tripList: any[] = [];
  startAdd: any;
  endAdd: any;
  addkey1: any;
  addkey2: any;
  skip: number = 1;
  limit: number = 10;
  _tripListKey: any[] = [];

  constructor(
    public apicall: ApiServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.getdevices();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YourTripsPage');
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicall.startLoading().present();
    this.apicall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apicall.stopLoading();
          console.log(err);
        });
  }

  getID(val) {
    this.trip_id = val._id;
  }

  getTrips(infiniteScroll) {
    var url;
    let that = this;

    if (!infiniteScroll) {
      this._tripListKey = [];
      this._tripList = [];

    }

    if (this.trip_id != undefined) {
      url = "https://www.oneqlik.in/user_trip/getLastTrip?device=" + this.trip_id + "&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
    } else {
      url = "https://www.oneqlik.in/user_trip/getLastTrip?&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
    }
    if (!infiniteScroll) {
      this.apicall.startLoading().present();
      this.apicall.getSOSReportAPI(url)
        .subscribe(data => {
          this.apicall.stopLoading();
          console.log("trip data: ", data)
          debugger
          if (!data.message) {
            this.innerFunc(data, infiniteScroll);
          } else {
            let toast = this.toastCtrl.create({
              message: "Trip(s) not found..!",
              duration: 1500,
              position: "bottom"
            })
            toast.present();
          }

        },
          err => {
            this.apicall.stopLoading();
          })
    } else {
      this.apicall.getSOSReportAPI(url)
        .subscribe(data => {
          console.log("trip data: ", data)
          debugger
          if (!data.message) {
            this.innerFunc(data, infiniteScroll);
          }
        })
    }
  }

  innerFunc(data, infiniteScroll) {
    let outerthis = this;
    outerthis.startAdd = undefined;
    outerthis.endAdd = undefined;
    var i = 0, howManyTimes = data.length;
    function f() {
      var date1 = moment(new Date(data[i].start_time), 'DD/MM/YYYY').format("LLLL");
      var str = date1.split(', ');
      var date2 = str[0];
      var date3 = str[1].split(' ');
      var date4 = str[2].split(' ');
      var date5 = date4[0];
      var date6 = date3[0];
      var date7 = date3[1];
      var date8 = date6 + ' ' + date5;
      var date9 = date4[1];
      outerthis._tripList.push({
        "date1": date2,
        "date2": date8,
        "date3": date7,
        "dtime": date9,
        "trip_name": data[i].trip_name,
        "trip_status": data[i].trip_status
      });
      if (data[i].end_lat != null && data[i].start_lat != null) {
        var latEnd = data[i].end_lat;
        var lngEnd = data[i].end_long;
        var latlng = new google.maps.LatLng(latEnd, lngEnd);

        var latStart = data[i].start_lat;
        var lngStart = data[i].start_long;
        var latlng1 = new google.maps.LatLng(latStart, lngStart);

        var geocoder = new google.maps.Geocoder();
        var geocoder1 = new google.maps.Geocoder();

        var request = {
          latLng: latlng
        };
        var request1 = {
          latLng: latlng1
        };
        var city1;
        geocoder.geocode(request, function (data, status) {
          debugger
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              var component = data[1].address_components[0];
              outerthis.endAdd = data[1].formatted_address;
              // var drvar = data[1].address_components[2].short_name;
              // var add1 = drvar.split(' ');
              // outerthis.addkey1 = add1[0];
              if (component.types[0] === 'locality') {
                city1 = component.long_name;
                outerthis.addkey1 = city1;
              } else {
                city1 = "N/A";
                outerthis.addkey1 = city1;
              }
            }
          }
          outerthis._tripList[outerthis._tripList.length - 1].eaddress = outerthis.endAdd;
          outerthis._tripList[outerthis._tripList.length - 1].eaddkey = outerthis.addkey1;
        })
        var city;
        geocoder1.geocode(request1, function (data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              debugger
              var component = data[1].address_components[0];
              outerthis.startAdd = data[1].formatted_address;
              // var drvar1 = data[1].address_components[2].short_name;
              // var add2 = drvar1.split(' ');
              // outerthis.addkey2 = add2[0];
              if (component.types[0] === 'locality') {
                city = component.long_name;
                outerthis.addkey2 = city;
              } else {
                city = "N/A";
                outerthis.addkey2 = city;
              }
            }
          }
          outerthis._tripList[outerthis._tripList.length - 1].saddress = outerthis.startAdd;
          outerthis._tripList[outerthis._tripList.length - 1].saddkey = outerthis.addkey2;
        })

      } else {

        outerthis._tripList[outerthis._tripList.length - 1].eaddress = 'N/A';
        outerthis._tripList[outerthis._tripList.length - 1].saddress = 'N/A';
        outerthis._tripList[outerthis._tripList.length - 1].saddkey = 'N/A';
        outerthis._tripList[outerthis._tripList.length - 1].eaddkey = 'N/A';
      }
      debugger
      if (infiniteScroll) {
        outerthis._tripListKey.push(outerthis._tripList);
        // infiniteScroll.complete();
      } else {
        // infiniteScroll.complete();
        outerthis._tripListKey = outerthis._tripList;
      }
      console.log(outerthis._tripListKey);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.skip = that.skip + 1;
    setTimeout(() => {
      that.getTrips(infiniteScroll)
      infiniteScroll.complete();
    }, 200);
  }

}
